#include "mem.h"
#include "common.h"
#include "mem_os.h"
#include <stdio.h>

//-------------------------------------------------------------
// mem_init
//-------------------------------------------------------------
struct fb {
    size_t sizefb ;
    struct fb *next ;
};

struct bb {
    size_t sizebb ;
};

struct entete {
    struct fb *premier_bloc_vide;
};

static struct entete *EnteteMem;
static int mode_alloc = 0

void mem_init() {
    // Création de l'entète de notre mémoire
    struct entete *EnteteMem = get_memory_adr();
    EnteteMem->premier_bloc_vide = get_memory_adr() + sizeof(struct entete);

    // Création de l'header de notre premier et pour l'instant seul bloc vide
    struct fb *premier_bloc_vide = get_memory_adr() + sizeof(struct entete);
    premier_bloc_vide->sizefb = get_memory_size() - sizeof(struct entete) - sizeof(struct fb);
    premier_bloc_vide->next = NULL;

    //on initie le mode d'allocation
    mem_fit(mem_first_fit);
    return;
}

void *mem_alloc(size_t size) {
    //on crée un pointeur vers NULL puis on lui attribue l'adresse d'un bloc libre selon la méthode choisie
    void *bloc_choisi = NULL;
    if (mode_alloc == 1) {
        bloc_choisi = mem_first_fit(EnteteMem->premier_bloc_vide, size);
    }
    else if (mode_alloc == 2) {
        bloc_choisi = mem_best_fit(EnteteMem->premier_bloc_vide, size);
    }
    else if (mode_alloc == 3) {
        bloc_choisi = mem_worst_fit(EnteteMem->premier_bloc_vide, size);
    }
    if (bloc_choisi == NULL)
    {
            return NULL;
    }
    //on rreturn l'adresse choisie en comptant le header de notre nouveau bloc occupé
    return (void*)((char *)bloc_choisi + sizeof(struct bb));
}

//-------------------------------------------------------------
// mem_free
//-------------------------------------------------------------
void mem_free(void *zone) {
    struct fb *bloc_courant = EnteteMem->premier_bloc_vide;
    struct fb *bloc_precedent = NULL;
    struct fb *nouveau_libre;

    if (zone == NULL) {
         return;
    }

    size_t taille_nouveau_libre = ((struct bb*)zone)->sizebb + sizeof(struct bb) - sizeof(struct fb); //on calcule la taille de notre bloc libre
    nouveau_libre = (struct fb*)((void *)zone - sizeof(struct bb));

    //on parcours les blocs libres jusqu'à arriver à celui que l'on veut libérer.
    while (bloc_courant != NULL)
    {
        if (nouveau_libre < bloc_courant)
        {
            if (bloc_precedent != NULL) {
                bloc_precedent->suivant = nouveau_libre;
            }
            else {
                EnteteMem->premier_bloc_vide = nouveau_libre; 
            }
            nouveau_libre->suivant = bloc_courant;
            nouveau_libre->sizefb = taille_nouveau_libre;
            break;
        }

        bloc_precedent = bloc_courant;
        bloc_courant = bloc_courant->suivant;
    }

    //si l'on parcours toute la liste et que l'on ne dépasse pas l'adresse que l'on cherche a libérer, alors on le place a la fin
    if (bloc_courant == NULL) {
        EnteteMem->premier_bloc_vide = nouveau_libre;
        nouveau_libre->next = NULL;
        nouveau_libre->sizefb = taille_nouveau_libre;

    }

    return;
}

//-------------------------------------------------------------
// Itérateur(parcours) sur le contenu de l'allocateur
// mem_show
//-------------------------------------------------------------
void mem_show(void (*print)(void *, size_t size, int free)) {
    struct entete *EnteteMem = get_memory_adr();
    void *bloc_libre_cour = EnteteMem->premier_bloc_vide; //on definie le bloc libre courant
    void *bloc_cour = EnteteMem + sizeof(struct entete); //on definie le bloc libre, qu'il soit courant ou non
    
    //On boucle jusqu'à arriver à la fin de notre mémoire
    while (bloc_libre_cour < get_memory_adr() + get_memory_size() && bloc_cour < get_memory_adr() + get_memory_size()) {
        //si le bloc courant est libre 
        if (bloc_libre_cour - bloc_cour == 0) {
            print(bloc_libre_cour, ((struct fb*)bloc_libre_cour)->sizefb, 1);
            bloc_cour = bloc_cour + ((struct fb*)bloc_libre_cour)->sizefb + sizeof(struct fb);
            bloc_libre_cour = ((struct fb*)bloc_libre_cour)->next;
        }
        //si le bloc courant n'est pas libre
        else {
            print(bloc_cour, ((struct bb*)bloc_cour)->sizebb, 0);
            bloc_cour = bloc_cour + ((struct bb*)bloc_cour)->sizebb + sizeof(struct bb);
        }
    }
    return;
}

//-------------------------------------------------------------
// mem_fit
//-------------------------------------------------------------
void mem_fit(mem_fit_function_t *mff) {
    if (mff == mem_first_fit) {
        mode_alloc = 1;
    }
    else if (mff == mem_best_fit) {
        mode_alloc = 2;
    }
    else if (mff == mem_worst_fit) {
        mode_alloc = 3;
    }
    return;
}

//-------------------------------------------------------------
// Stratégies d'allocation
//-------------------------------------------------------------
struct fb *mem_first_fit(struct fb *head, size_t size) {
    struct entete *EnteteMem = get_memory_adr();
    struct fb *bloc_courant = EnteteMem->premier_bloc_vide;
    struct fb *bloc_precedent = NULL;
    struct fb *bloc_choisi = NULL;
    struct fb *debut_suite = NULL;
    struct fb *fin_suite = NULL;
    struct fb *avant_suite = NULL;
    size_t taille_suite = 0;

    while (bloc_courant!= NULL) //on parcours la liste jusqu'à la fin, ou l'on break lorsque l'on trouve un bloc ou groupe de blocs de taille suffisante
    {
        if (fin_suite != NULL && (void *)fin_suite + sizeof(struct fb) + fin_suite->sizefb == (void *)bloc_courant)
        {
            fin_suite = bloc_courant;
            taille_suite += sizeof(struct fb) + bloc_courant->sizefb;
        }
        else
        {
            avant_suite = bloc_precedent;
            debut_suite = bloc_courant;
            fin_suite = bloc_courant;
            taille_suite = bloc_courant->sizefb;
        }

        if (taille_suite >= size)
        {
            //on différencie deux cas, si le bloc vide choisi est assez grand pour accueillir notre bloc occupé plus un nouveau bloc vide ou non
            if (taille_suite > size + sizeof(struct bb) + sizeof(struct fb))
            {
                struct fb *nouveau_libre = NULL;
                //on différencie a nouveau selon si le bloc précédant celui choisi est l'entete ou non 
                if (avant_suite != NULL) {
                    avant_suite->next = debut_suite + sizeof(struct bb) + size;
                    nouveau_libre = avant_suite->next;
                }
                else {
                    EnteteMem->premier_bloc_vide = debut_suite + sizeof(struct bb) + size;
                    nouveau_libre = EnteteMem->premier_bloc_vide;
                }
                nouveau_libre->next = fin_suite->next;
                nouveau_libre->sizefb = taille_suite - size - sizeof(struct bb) - sizeof(struct fb);
                ((struct bb*)debut_suite)->sizebb = size;
                bloc_choisi = debut_suite;
                break;
            }
            else {
                if (avant_suite != NULL) {
                    avant_suite->next = fin_suite->next;
                }
                else {
                    EnteteMem->premier_bloc_vide = fin_suite->next;
                }

                ((struct bb*)debut_suite)->sizebb = taille_suite;
                bloc_choisi = debut_suite;
                break;
            }
        }
        //itérations pour se déplacer dans notre liste de blocs vides
        bloc_precedent = bloc_courant;
        bloc_courant = bloc_courant->next;
    }

    return bloc_choisi;
}

//-------------------------------------------------------------
struct fb *mem_best_fit(struct fb *head, size_t size) {
    struct entete *EnteteMem = get_memory_adr();
    struct fb *bloc_courant = EnteteMem->premier_bloc_vide;
    struct fb *bloc_precedent = NULL;
    struct fb *bloc_choisi = NULL;
    struct fb *debut_suite = NULL;
    struct fb *fin_suite = NULL;
    struct fb *avant_suite = NULL;
    struct fb *debut_suite_choisie = NULL;
    struct fb *fin_suite_choisie = NULL;
    struct fb *avant_suite_choisie = NULL;
    size_t taille_suite = 0;
    size_t taille_choisie = get_memory_size(); //on initie une taille suffisament grande pour que le premier bloc libre de taille suffisante soit ensuite choisi car il sera forcement plus petit
    
    //idem que pour first fit, mais on parcours forcement tous les blocs libres (donc pas de break), et on garde que le plus petit / le plus petit groupe
    while (bloc_courant!= NULL)
    {
        if (fin_suite != NULL && (void *)fin_suite + sizeof(struct fb) + fin_suite->sizefb == (void *)bloc_courant)
        {
            fin_suite = bloc_courant;
            taille_suite += sizeof(struct fb) + bloc_courant->sizefb;
        }
        else
        {
            avant_suite = bloc_precedent;
            debut_suite = bloc_courant;
            fin_suite = bloc_courant;
            taille_suite = bloc_courant->sizefb;
        }

        if (taille_suite >= size && taille_suite < taille_choisie) {
            avant_suite_choisie = avant_suite;
            debut_suite_choisie = debut_suite;
            fin_suite_choisie = fin_suite;
            taille_choisie = taille_suite;
        }

        bloc_precedent = bloc_courant;
        bloc_courant = bloc_courant->next;
    }

    avant_suite = avant_suite_choisie;
    debut_suite = debut_suite_choisie;
    fin_suite = fin_suite_choisie;
    taille_suite = taille_choisie;

    if (taille_suite > size + sizeof(struct bb) + sizeof(struct fb))
    {
        struct fb *nouveau_libre = NULL;
        if (avant_suite != NULL) {
            avant_suite->next = debut_suite + sizeof(struct bb) + size;
            nouveau_libre = avant_suite->next;
        }
        else {
            EnteteMem->premier_bloc_vide = debut_suite + sizeof(struct bb) + size;
            nouveau_libre = EnteteMem->premier_bloc_vide;
        }
        nouveau_libre->next = fin_suite->next;
        nouveau_libre->sizefb = taille_suite - size - sizeof(struct bb) - sizeof(struct fb);
        ((struct bb*)debut_suite)->sizebb = size;
        bloc_choisi = debut_suite;
    }
    else {
        if (avant_suite != NULL) {
            avant_suite->next = fin_suite->next;
        }
        else {
            EnteteMem->premier_bloc_vide = fin_suite->next;
        }
        ((struct bb*)debut_suite)->sizebb = taille_suite;
        bloc_choisi = debut_suite;
    }

    return bloc_choisi;
}
//-------------------------------------------------------------
struct fb *mem_worst_fit(struct fb *head, size_t size) {
    struct entete *EnteteMem = get_memory_adr();
    struct fb *bloc_courant = EnteteMem->premier_bloc_vide;
    struct fb *bloc_precedent = NULL;
    struct fb *bloc_choisi = NULL;
    struct fb *debut_suite = NULL;
    struct fb *fin_suite = NULL;
    struct fb *avant_suite = NULL;
    struct fb *debut_suite_choisie = NULL;
    struct fb *fin_suite_choisie = NULL;
    struct fb *avant_suite_choisie = NULL;
    size_t taille_suite = 0;
    size_t taille_choisie = 0; //on initie à 0 pour que le premier bloc de taille suffisante soit selectionné car il sera forcement plus grand
    
    //idem que pour best fit mais avec une taille maximale
    while (bloc_courant!= NULL)
    {
        if (fin_suite != NULL && (void *)fin_suite + sizeof(struct fb) + fin_suite->sizefb == (void *)bloc_courant)
        {
            fin_suite = bloc_courant;
            taille_suite += sizeof(struct fb) + bloc_courant->sizefb;
        }
        else
        {
            avant_suite = bloc_precedent;
            debut_suite = bloc_courant;
            fin_suite = bloc_courant;
            taille_suite = bloc_courant->sizefb;
        }

        if (taille_suite >= size && taille_suite > taille_choisie) {
            avant_suite_choisie = avant_suite;
            debut_suite_choisie = debut_suite;
            fin_suite_choisie = fin_suite;
            taille_choisie = taille_suite;
        }

        bloc_precedent = bloc_courant;
        bloc_courant = bloc_courant->next;
    }

    avant_suite = avant_suite_choisie;
    debut_suite = debut_suite_choisie;
    fin_suite = fin_suite_choisie;
    taille_suite = taille_choisie;

    if (taille_suite > size + sizeof(struct bb) + sizeof(struct fb))
    {
        struct fb *nouveau_libre = NULL;
        if (avant_suite != NULL) {
            avant_suite->next = debut_suite + sizeof(struct bb) + size;
            nouveau_libre = avant_suite->next;
        }
        else {
            EnteteMem->premier_bloc_vide = debut_suite + sizeof(struct bb) + size;
            nouveau_libre = EnteteMem->premier_bloc_vide;
        }
        nouveau_libre->next = fin_suite->next;
        nouveau_libre->sizefb = taille_suite - size - sizeof(struct bb) - sizeof(struct fb);
        ((struct bb*)debut_suite)->sizebb = size;
        bloc_choisi = debut_suite;
    }
    else {
        if (avant_suite != NULL) {
            avant_suite->next = fin_suite->next;
        }
        else {
            EnteteMem->premier_bloc_vide = fin_suite->next;
        }
        ((struct bb*)debut_suite)->sizebb = taille_suite;
        bloc_choisi = debut_suite;
    }

    return bloc_choisi;
}

